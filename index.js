const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
const dbHandler = require("./db.js");
const Post = require("./models/post.js");

dbHandler.connectToDb();

app.get("/", function (req, res) {
  res.send("Hello World!");
});

app.get("/posts", function (req, res) {
  //   console.log("Searching posts for username " + req.headers.username);
  //   Post.find({ user: req.headers.username }, function (err, posts) {
  //     res.json({ posts: posts });
  //   });
  Post.find({}, function (err, posts) {
    res.json({ posts: posts });
  });
});

app.get("/posts/id/:id", function (req, res) {
  Post.find({ _id: req.params.id }, function (err, post) {
    res.json({ post });
  });
});

app.post("/posts/remove", function (req, res) {
  //   let postToDelete;
  //   Post.find({ _id: req.body.id }, function (err, post) {
  //     postToDelete = post;

  //     if (postToDelete) {
  //       console.log("Found " + postToDelete);
  //       console.log(postToDelete.user + " " + req.headers.username);
  //       if (postToDelete.user === req.headers.username) {
  Post.findOne({ _id: req.body.id }, function (err, user) {
    user.remove();
    res.send("Deleted");
  });
  //     res.send("Deleted");
  //   } else {
  //     res.send("Cant");
  //   }
  // }
  //   });
});

app.post("/posts/add", function (req, res) {
  const newPost = new Post(req.body);

  newPost.save((err, result) => {
    if (err) console.log(err);
    console.log(result);
  });
  res.send("Post added");
});

app.listen(process.env.PORT || 5000, function () {
  console.log("Example app listening on port 3000!");
});
