const mongoose = require("mongoose");
const Scheme = mongoose.Schema;

const ItemSchema = new Scheme({
  content: {
    type: String,
    require: true,
  },
  user: {
    type: String,
    require: true,
  },
  date: {
    type: Date,
    require: false,
    default: Date.now(),
  },
  isLiked: {
    type: Boolean,
    require: false,
    default: false,
  },
});

module.exports = Post = mongoose.model("Post", ItemSchema);
