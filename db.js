const connectToDb = () => {
  const mongoose = require("mongoose");

  const url = `mongodb+srv://facetagram:Mm123456@cluster0.z6lvb.mongodb.net/vueExrcFacetagram?retryWrites=true&w=majority`;

  const connectionParams = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  };
  mongoose
    .connect(url, connectionParams)
    .then(() => {
      console.log("Connected to database ");
    })
    .catch((err) => {
      console.error(`Error connecting to the database. \n${err}`);
    });
};

module.exports = { connectToDb };
